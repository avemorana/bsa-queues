<?php

namespace App\Jobs;

use App\Services\PhotoService;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class CropJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $fileName;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(string $fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        $path = 'storage/app/public/images/' . Auth::id() . '/' . $this->fileName;
        $file100 = PhotoService::crop($path, 100, 100);
        $file150 = PhotoService::crop($path, 150, 150);
        $file250 = PhotoService::crop($path, 250, 250);

        $baseName = File::basename($this->fileName);
        $extension = File::extension($this->fileName);

        $path100 = 'storage/app/public/images/' . Auth::id() . '/' . $baseName . '100x100.' . $extension;
        $path150 = 'storage/app/public/images/' . Auth::id() . '/' . $baseName . '150x150.' . $extension;
        $path250 = 'storage/app/public/images/' . Auth::id() . '/' . $baseName . '250x250.' . $extension;

        Storage::move($file100, $path100);
        Storage::move($file150, $path150);
        Storage::move($file250, $path250);

        Photo::create([
            'user_id' => Auth::id(),
            'original_photo' => $path,
            'photo_100_100' => $path100,
            'photo_150_150' => $path100,
            'photo_250_250' => $path100,
            'status' => 'UPLOADED'
        ]);
    }
}
