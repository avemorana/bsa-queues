<?php

namespace App\Http\Controllers;

use App\Jobs\CropJob;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class PhotoController extends Controller
{
    private const PATH_TO_UPLOAD = 'images/';

    public function upload(Request $request)
    {
        $uploadFile = $request->file('photo');
        $fileName = $uploadFile->getClientOriginalName();
        $uploadFile->storeAs(self::PATH_TO_UPLOAD . Auth::id() . '/',  $fileName);

        CropJob::dispatch($fileName);

        return new Response($fileName);
    }

    public function __construct()
    {
        $this->middleware('auth');
    }
}
